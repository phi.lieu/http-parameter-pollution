public class ActionItem
{
    public long Id { get; set; }
    public string Action { get; set; }
    public int Amount { get; set; }
}
